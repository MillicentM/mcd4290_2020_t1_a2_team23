/*// Code for the Measure Run page.

/*
Task 2

The app should locate the user and display their position and location accuracy on an interactive map. This information should be updated as the user’s location changes or increases in accuracy.

*/

var actualLocation; 
var longitudeValue;
var latitudeValue;
var accuracyValue;
var randomLongitude;
var randomLatitude;
var new_longitude; 
var new_latitude; 
var distanceBetween_twoPoint;
var intervalLocation;
// empty arrays of path taken by user
 // Task 4
var endDate;
var startDate;
var timeRecording
var PathTimeDistance;
var instanceOfRun =new Run();
var startLocation;
var desLocation;
var distance
var pathTaken=[]
var APP_PREFIX = "monash.mcd4290.runChallengeApp";
var Storage_selection = "-selectedRun"







        
mapboxgl.accessToken = 'pk.eyJ1IjoibWNkNDI5MHRlYW0yMyIsImEiOiJjazh6c2d3a2Ixd2NvM21qc3BtZXdieHE4In0.J7tVV-uVFBIHY0WN_xSCPw';

//For Task 9
var reAttempt=JSON.parse(localStorage.getItem("Reattempt"));

//Condition to verify if it is reattempting or creating new run


if (reAttempt !==null)
    {
        
        alert ("User must be close to the initial position of the Run in order for it to proceed smoothly")
        
        actualLocation=reAttempt.startLocation; //Initial Location
        //console.log(actualLocation)
        longitudeValue=reAttempt.startLocation[0]; //Initial long
        latitudeValue=reAttempt.startLocation[1]; //Initial Lat
        
        newCoordinates=reAttempt.desLocation
        new_longitude=reAttempt.desLocation[0];
        new_latitude=reAttempt.desLocation[1];
        // console.log(newCoordinates)
        pathTaken=reAttempt.pathTaken;
        estimatedDistance=reAttempt.distance
            //console.log(estimatedDistance)
        
         document.getElementById("randomDestination").disabled=false;

        
         let map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v10',
            zoom: 16,
            center: actualLocation 
            });   
    
    locations =[{ position: actualLocation, label: "Start Location" }, { position: newCoordinates, label: "Destination Location" }];   
        
        //console.log(locations)
       
        //For the initial location 
        let marker4= new mapboxgl.Marker({color: "purple"})
        marker4.setLngLat(actualLocation)
        marker4.addTo(map)
    
        let popup4= new mapboxgl.Popup({offset:40})
        popup4.setText("Start Location");
        marker4.setPopup(popup4);
        popup4.addTo(map)
        
        
        
        //For the final location 
        let marker5= new mapboxgl.Marker({color: "pink"})
        marker5.setLngLat(newCoordinates)
        marker5.addTo(map)
    
        let popup5= new mapboxgl.Popup({offset:40})
        popup5.setText("Final Location");
        marker5.setPopup(popup5);
        popup5.addTo(map)
        
        
        document.getElementById('estimatedDistance').innerHTML="Estimated Distance: " + estimatedDistance.toFixed(4) +" meters"
        

        
       instanceOfRun.setstartLocation(actualLocation)
        instanceOfRun.setdesLocation(newCoordinates) 
    }
   

    
navigator.geolocation.watchPosition(showCurrentLocation,errorHandler)

    








//centerDefault location (in case current location doesnt work)


//End of Map.html-

//Create functions for watchPosition() (LINK: https://developer.mozilla.org/en-US/docs/Web/API/Geolocation/watchPosition)
 
//Success: callback function that takes a GeolocationPosition object as an input parameter. 
function showCurrentLocation(actualPosition) 
{
    // Shows current lat and long of user
    map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v10',
     zoom: 16,
    center: actualLocation 
                            }); 
            
     map.panTo(actualLocation)
    
    
    longitudeValue=Number(actualPosition.coords.longitude)
    latitudeValue=Number(actualPosition.coords.latitude)
    accuracyValue= actualPosition.coords.accuracy
    
    actualLocation = [longitudeValue,latitudeValue]
    //console.log(longitudeValue)
    //console.log(latitudeValue)
    
    console.log(actualLocation)    
    /* var map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v10',
            zoom: 16,
            center: actualLocation
        }); */

//To create a red mark to the current location    
     let marker= new mapboxgl.Marker({color: "purple"})
        marker.setLngLat(actualLocation)
        marker.addTo(map)
    
     let popup= new mapboxgl.Popup({offset:40})
        popup.setText("Current Location");
        marker.setPopup(popup);
        popup.addTo(map)
    
    
if (accuracyValue<=20)
    {
        document.getElementById("randomDestination").disabled=false;
    }
else
    {
        document.getElementById("randomDestination").disabled=true;
    }
    
    
}



//Error: An optional callback function that takes a GeolocationPositionError object as an input parameter. 

function errorHandler (error) 
{
    if (error.code===1)
        {
            alert("The Location access was denied by the user")
        }
    else if (error.code ===2)
        {
            alert("Location is unavailable")
        }
    else if (error.code===3)
        {
            alert("Access of location timed out")
        }
    else
        {
            alert("An unknown error has been detected at the time of getting location")
        }
}

                                                                                    


//Task 3: Random destinations:

var newCoordinates; 

function randomCoordinates()
{
    //console.log(longitudeValue)
    //console.log(latitudeValue)
    //console.log(actualLocation)
    //To get random values
    
     new_longitude = ((-0.001+0.002*Math.random())+longitudeValue);
     new_latitude = ((-0.001+0.002*Math.random())+latitudeValue);
    newCoordinates=[new_longitude,new_latitude]
    //console.log(new_longitude)
    //console.log(new_latitude)
    //console.log(newCoordinates)
}


//To calculate distance between two locations:

const radius_earth=6371e3; //meters

function distanceBetweenTwoPoints()
{
    //We will use the Harvesine Formula for this. We will divide the formula into THREE parts in order to decrease the difficulty of coding it.
            //Source used: http://movable-type.co.uk/scripts/latlong.html
    
    //Longitude in Rad
        const long_rad_1=longitudeValue*Math.PI/180;//current long
        const long_rad_2=new_longitude*Math.PI/180;//new random long
            //console.log(long_rad_1)
            //console.log(long_rad_2)

    //Latitude in Rad
        const lat_rad_1=latitudeValue*Math.PI/180;//current lat
        const lat_rad_2=new_latitude*Math.PI/180;//new random lat

    //Delta Value Longitude and Lat
        const deltaLong=(new_latitude-latitudeValue)*Math.PI/180;
        const deltaLat=(new_longitude-longitudeValue)*Math.PI/180;
        
    
    
    //1st Part: Solving Inside the brackets:
    const part_1=Math.sin(deltaLat/2)*Math.sin(deltaLat/2)+Math.cos(lat_rad_1)*Math.cos(lat_rad_2)*Math.sin(deltaLong/2)*Math.sin(deltaLong/2)
        //console.log(part_1)
    
    //2nd Part: 2 x arctan (sqrt(part 1),sqrt(1-part1))
    const part_2=2* Math.atan2(Math.sqrt(part_1),Math.sqrt(1-part_1))
    
    //3rd Part: Finding the distance between two points
    distanceBetween_twoPoint=radius_earth*part_2 //Will give solution in meters
    //console.log(distanceBetween_twoPoint)
    console.log(distanceBetween_twoPoint)
    return distanceBetween_twoPoint
      
}



//Creation of array of the marker and popup 2:
var marker_2;
var popUp_2;


function randomDestination()
{ //declared before as true
    
//Calling previous functions done
let conditionToUseWhileLoop=true; //Conditions used only for the use of a while loop
    randomCoordinates();
    distanceBetweenTwoPoints();
    
    
    if (marker_2 !=null)
        {
            if (popUp_2 !=null)
                {
                   marker_2.remove();
                        marker_2=null;
                    popUp_2.remove();
                        popUp_2=null;
                }
        }
    
    while (conditionToUseWhileLoop===true)
        {
        //60m is the minimum distance and 150m is the max distance from the current location
            if (distanceBetween_twoPoint>=60)
            {
                if (distanceBetween_twoPoint<=150)
                {
                    newCoordinates=[new_longitude,new_latitude];
                    //console.log(newCoordinates)

                    marker_2=new mapboxgl.Marker({color:'pink'})
                    marker_2.setLngLat(newCoordinates)
                    marker_2.addTo(map)
                
                    popUp_2=new mapboxgl.Popup({offset:40})
                    popUp_2.setLngLat(newCoordinates)
                    popUp_2.setText('Final Location')
                    popUp_2.addTo(map)
                            
                    
             //For the estimated distance (the one calculated)  to show in website when button is pressed  
               let estimatedDistance=distanceBetween_twoPoint.toFixed(4); //value in km
               //Display the value
               document.getElementById('estimatedDistance').innerHTML="Estimated Distance: " + estimatedDistance +" meters"
                    
                    //change of conditionToUseWhileLoop to false in order to "Stop" loop
                    conditionToUseWhileLoop=false;
                }
            }
            else 
                {
                    //Calling the functions above again to repeat process until the conditions are achieved.
                    randomCoordinates();
                    distanceBetweenTwoPoints();


                    
                    conditionToUseWhileLoop=true; //Such as initial conditions
                }
        }
    
}

  //Check the distance from destination



function addIn()
{
        // push the current location to Array    
        pathTaken.push(actualLocation);
        //console.log(pathTaken)
        PathTimeDistance = distanceBetweenTwoPoints(actualLocation[0],actualLocation[1],newCoordinates[0],newCoordinates[1]);
        console.log("destinaton location  "+  PathTimeDistance)
    }



//Initiate the following actions when 'Begin Run' is pressed
function beginRun()
{
    document.getElementById("randomDestination").disabled=false;
    addIn();
    
    intervalLocation = setInterval(addIn,5000)

    startDate = new Date();
    //Save new this new run instance in a class along with some starting infos
    instanceOfRun = new Run();
    // Set value to parameter of start location
    instanceOfRun.setstartLocation (startLocation);
    // set value to parameter of destination location
    instanceOfRun.setdesLocation (desLocation);
    instanceOfRun.setstartDate (startDate);
}



//AUTOMATIVALLY
if (distanceBetween_twoPoint<10)
    {

        // if the discance is less than 10 meter , stop run

        endRun();
    }





//User's choice
function endRun()
{
  
    
    clearInterval(intervalLocation);

    startLocation=actualLocation;
    desLocation=newCoordinates;
    
    
    
    clearInterval(intervalLocation);
    //Track the end time
    endDate = new Date();
    // How much time used
    timeRecording = (endDate-startDate);
    console.log("time taken"+timeRecording/1000+"s"); //divided by 1000 to convert from ms to seconds
    document.getElementById("timeTaken").innerHTML = "Time Taken:"+(timeRecording/1000).toFixed(4)+" Seconds";
    
    // the distance between current location to destination
    distance = distanceBetweenTwoPoints(actualLocation[0],actualLocation[1],newCoordinates[0],newCoordinates[1]);
    //console.log("Distance between two points"+distance);
    document.getElementById("distanceBetween2Point").innerHTML = "Distance Left: "+ distance.toFixed(4)+" meters";
    // save more information to Run class
    instanceOfRun.setdistance (distance)
    instanceOfRun.setpathTaken (pathTaken);
    instanceOfRun.setendDate (endDate);
    instanceOfRun.settimeRecording (timeRecording);
    instanceOfRun.setdesLocation(desLocation);
    instanceOfRun.setstartLocation(startLocation);
    //console.log(desLocation)
    //document.getElementById('saveRun').disabled = false;  
}


function saveRun()
{
    if(typeof(Storage) !== 'undefined')
        {
            var runArray = []
            runArray.push(instanceOfRun)
            
            console.log(runArray)
           let storeInf = JSON.stringify(runArray)
            
         localStorage.setItem(APP_PREFIX+"-selectedRun",storeInf)
            //location.href = 'index.html'
           
        }else
            {
                console.log("Error: LocalStorage is not supposrted by current brower")
            }
    instanceOfRun = null;
   window.location.href = "index.html"
   
            
        
} 

