// Shared code needed by all three pages.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.mcd4290.runChallengeApp";
var Storage_selection = "-selectedRun"

// Array of saved Run objects.
var savedRuns = [];
var runArray = [];
var instanceOfRun;
var RunObj
var RunJSON

class Run
{
	constructor(
	startLocation, desLocation, pathTaken, startDate, endDate,timeRecording,distance)
	{
		this.startLocation= startLocation;
		this.desLocation= desLocation;
		this.pathTaken= pathTaken;
		//empty Array
		this.startDate= startDate;
		this.endDate= endDate;
        this.timeRecording= timeRecording;
        this.distance= distance;
	}


//Class for startLocation, desLocation, startDate for task 4
// Mutator: set, Accessor: get
//startLocation ==================================
    
setstartLocation (startLocation)
    {
        this.startLocation= startLocation;
    }
getstartLocation()
    {
        return this.startLocation
    }
    
// startDate ==================================
setstartDate (startDate)
    {
        this.startDate= startDate;
    }
getstartDate()
    {
        return this.startDate
    }

//desLocation ==================================
setdesLocation (desLocation)
    {
        this.desLocation= desLocation;
    }
getdesLocation()
    {
        return this.desLocation
    }
    
//Class for pathTaken, endDate, time recording and distance
    
//pathTaken ==================================
setpathTaken (pathTaken)
    {
        this.pathTaken= pathTaken;
    }
getpathTaken()
    {
        return this.pathTaken
    }

//endDate ==================================
setendDate (endDate)
    {
        this.endDate= endDate;
    }
getendDate()
    {
        return this.endDate
    }
    
//timeRecording ==================================
settimeRecording (timeRecording)
    {
        this.timeRecording= timeRecording;
    }
gettimeRecording()
    {
        return this.timeRecording
    }
    
//distance ==================================
setdistance (distance)
    {
        this.distance= distance;
    }
getdistance()
    {
        return this.distance
    }
initialisePDO(runObj)
    {
        this.startLocation=runObj.startLocation
        this.desLocation=runObj.desLocation
        this.startDate = new Date(runObj.startDate);
        this.endDate = new Date(runObj.endDate);
        this.distance = runObj.distance
        this.pathTaken = []
        for(var i =0;i<runObj.pathTaken.length;i++)
            {
                this.pathTaken.push(runObj.pathTaken[i]);
            }
    }
}
function loadRuns()
{
    savedRuns = [];
    var loadRunObj = localStorage.getItem(APP_PREFIX)
    for(var i = 0;i<loadRunObj.length;i++)
        {
            savedRuns[i] = new Run();
            savedRuns[i].initialisePDO(loadRunObj[i]);
        }
}

function saveRuns()
{
    if(typeof(Storage)!== undefined) //check storage
{
    if(localStorage.getItem(APP_PREFIX) === null) // check local storage is empty
    {
        runArray.push(instanceOfRun);  // push the new class to array
                    
        var ArrayList = JSON.stringify(runArray);// convert string form to JSON 
        localStorage.setItem(APP_PREFIX+Storage_selection,RunJSON);
    }else{
          savedRuns = [];
                    //parse the local storage
          RunJSON = localStorage.getItem(APP_PREFIX+"-selectedRun");
          if(RunJSON){
         RunObj = JSON.parse(RunJSON);//convert JSON to string form
         for(var i = 0;i<RunObj.length;i++)
         {
          //loop to turn JSON objects back into run objects
          savedRuns[i] = new Run(RunObj[i].startLocation)
          savedRuns[i].initialisePDO(RunObj[i]);
         }
         }
         }
}
    
}



