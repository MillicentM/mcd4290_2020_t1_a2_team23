// Code for the View Run page.

// The following is sample code to demonstrate navigation.
// You need not use it for final app.

//Info about the run based on run Index
var listRuns= JSON.parse(localStorage.getItem(APP_PREFIX));

// get list of runs from the local storage

let output= "";
let features=document.getElementById('features');

//Show the map's done run
timeRecording=(listRuns.timeRecording)/1000

console.log(timeRecording)

startLocation=listRuns.startLocation
distance=listRuns.distance
pathTaken=listRuns.pathTaken
desLocation=listRuns.desLocation

mapboxgl.accessToken = 'pk.eyJ1IjoibWNkNDI5MHRlYW0yMyIsImEiOiJjazh6c2d3a2Ixd2NvM21qc3BtZXdieHE4In0.J7tVV-uVFBIHY0WN_xSCPw';

var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v10',
    zoom: 16,
    center: startLocation 
    }); 


locations=[{position: listRuns.startLocation, label: "Starting Point"}, {position:listRuns.desLocation, label: "Final Point"}];

function lineBetweenDots()
        {   
 
    map.addSource('route', {
        'type': 'geojson',
        'data': {
        'type': 'Feature',
        'properties': {},
        'geometry': {
        'type': 'LineString',
        'coordinates': [
                        [pathTaken]
                       ]
                }
            }
        });
     map.addLayer({
        'id': 'route',
        'type': 'line',
        'source': 'route',
        'layout': 
         {
        'line-join': 'round',
        'line-cap': 'round'
         },
         'paint': {
         'line-color': '#109',
         'line-width': 8
                  }
                    });

        }
    


//Add Markers and PopUps:
for (let i=0; i<locations.length;i++)
    {
        marker=new mapboxgl.Marker
        ({
            color: "lightgreen"
        });
        popup=new mapboxgl.Popup
        ({
            offset:40
        });
        
        position=locations[i].position;
        label= locations[i].label;
        
        marker.setLngLat(position);
        marker.addTo(map);
        
        popup.setText(label);
        marker.setPopup(popup);
        
        

    }

function deleteRun()
{
    
    let alert=confirm("Are you sure to delete this run? (This action cannot be undone)")
    if (alert)
        {
        for (i=0;i<listRuns.length;i++)
            {
            listRuns.splice(i,1);
            localStorage.setItem("runsList",JSON.stringify(listRuns));
            }
        }
            location.href="index.html"

}



if (listRuns !==null)
    {
        var runName=["Run A", "Run B"];
        document.getElementById("headerBarTitle").textContent=runName[listRuns]
    }




//Task 9: Repeat the Run if user wants to improve it
document.getElementById("reAttempt").disabled=false;

function reDoRun()
{
    //Set the item to the local storage so that it can be accessed in anothr page
    localStorage.setItem("Reattempt", JSON.stringify(listRuns));
    window.localStorage.removeItem(APP_PREFIX);
    location.href="newRun.html"
    
}

//To get the average speed of the user in that run
let duration=listRuns.endDate-listRuns.startDate
let average_speed=((distance)/(timeRecording)).toFixed(4);
//console.log(timeRecording)
//console.log(distance)
//console.log(average_speed)

//To show it in the webpage: Distance, Duration and speed:
output+= "Distance: " + distance+" m" + "<br>" + "Duration: "+ timeRecording + " s"+ "<br>" + "Average Speed " + average_speed + " m/s";

features.innerHTML=output;



