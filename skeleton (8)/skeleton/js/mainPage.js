// Code for the main app page (Past Runs list).


//To Show runs already done
var runsDone=JSON.parse(localStorage.getItem(APP_PREFIX+"-selectedRun"));
var runLists= document.getElementById("runsList");
let listHTML="";

//To show the information in the main page:
for (let i=0; i < runsDone.length;i++)
    {
        startLocation=runsDone[i].startLocation;
        //console.log(startLocation)
        
        desLocation=runsDone[i].desLocation;
        startDate=runsDone[i].startDate;
        endDate=runsDone[i].endDate;
        //console.log(endDate)
        distance=runsDone[i].distance;
        
        //console.log(startDate)
        //console.log(endDate)
        
        //To display:
        listHTML += "Run " + (i+1) + "<tr> <td onmousedown=\"viewRun(" + i + ")\" class=\"full-width mdl-data-table__cell--non-numeric\">"; 
        
        listHTML += "<div class=\"subtitle\">" + "Starting Time: " + startDate + "<br>" 
        listHTML += "Ending Time: " + endDate + "</div></td></tr>";
    }

runLists.innerHTML=listHTML;


//When click in the info of the run to have more information
function viewRun(runIndex)
{
    localStorage.setItem(APP_PREFIX,JSON.stringify(runsDone[runIndex]));
    
    //When click, will take to viewRun page
    location.href="viewRun.html"
}









